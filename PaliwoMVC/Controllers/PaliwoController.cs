﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaliwoMVC.Models;

namespace PaliwoMVC.Controllers
{
    public class PaliwoController : Controller
    {
        private PaliwoEntities db = new PaliwoEntities();

        // GET: Paliwo
        public ActionResult Index()
        {
            var paliwo = db.Paliwo.ToList().OrderByDescending(n => n.Data).Take(20);
            /*var paliwo2 = from n in paliwo
                          orderby n.Data descending
                          select n*/
            return View(paliwo);
        }


        public ActionResult Obliczanie(int? id)
        {
            var paliwoInDb = db.Paliwo.SingleOrDefault(p => p.Id == id);
            if (paliwoInDb == null)
                return View();

            return View(paliwoInDb);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(Paliwo paliwo)
        {
            if (!ModelState.IsValid)
                return View("Obliczanie", paliwo);

            paliwo.Data = DateTime.Now;
            db.Paliwo.Add(paliwo);
            db.SaveChanges();
            paliwo = db.Paliwo.ToList().Last();

            var wynik = (paliwo.Kilometry * paliwo.Spalanie) / 100;
            wynik *= paliwo.Cena;
            wynik /= paliwo.Osoby;
            if (paliwo.DwieStrony)
                ViewBag.Wynik = $"{wynik * 2} zł";
            else
                ViewBag.Wynik = $"{wynik} zł";

            return View("Obliczanie", paliwo); ;
        }
    }
}