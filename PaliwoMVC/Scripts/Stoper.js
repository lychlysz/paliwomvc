﻿var el = document.getElementById("stoper");
var red = document.getElementById("box");
var t, n, pos = 0;
var sec = 0;
var minute = 0;
var mili = 0;

function start() {
    t = setInterval(stoper, 10);
    n = setInterval(box, 10);
}

function stoper() {
    if (sec >= 60) {
        minute++;
        sec = 0;
        mili = 0;
    } else if (mili >= 100) {
        sec++;
        mili = 0;
    }
    el.innerHTML = minute + ":" + sec + ":" + mili;
    mili++;
}

function stop() {
    clearInterval(t);
    clearInterval(n);
}

function reset() {
    minute = 0;
    sec = 0;
    mili = 0;
    pos = 0;
    el.innerHTML = minute + ":" + sec + ":" + mili;
    red.style.marginLeft = pos + "px";
}
function box() {
    pos++;
    red.style.marginLeft = pos + "px";
    if (pos >= 100) {
        pos = 0;
    }
}
